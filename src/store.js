import { writable, derived, get } from "svelte/store";

export const user = (() => {
  const store = writable(null);
  return {
    subscribe:store.subscribe,
    checkAuth: () => {
      fetch(`${window.BACKEND_URL}/api/v1/profile/`, {
        credentials: "include",
      })
        .then((res) => {
          if(res.status !== 200) throw new Error(res.status)
          return res.json()
        })
        .then((data) => {
          store.set(data)
        })
        .catch(() => {});
    },
   
  };
})();

const translation = {
  "CLIP":{
    ru:"попади в клип к звездам",
  en:"Make it into the music video with the Stars"
  },
  "POINT_1":{
    ru:'Сними на видео исполнение песни «Любимый&nbsp;город»',
    en:'Shoot a video performing «Lubymyi&nbsp;Gorod» («Beloved&nbsp;City»)'
  },
  "POINT_2":{
    ru:'Опубликуй видео с хештегом <span style="color:#b59975">#споемвместе</span>',
    en:'Using <span style="color:#b59975">#singtogether</span> hashtag post your video online  (<span style="color:#b59975">#spoyiomvmeste</span>)'
  },
  "POINT_3":{
    ru:'Найди себя <a href="https://devyataev.film/#video" style="color:#b59975">на нашем сайте</a>',
    en:'Your video will appear on the site'
  },
  "POINT_4":{
    ru:'Делись ссылкой на исполнение и набирай голоса - самые популярные видео окажутся в клипе со звездами',
    en:'Share the link, get likes and pave your way into a music video with the Star.'
  },
  "BE_THE_BEST":{
    ru:'спой вместе с нами',
    en:'Join the contest and win!'
  },
  "CONDITIONS":{
    ru:'условия участия',
    en:'Conditions'
  },
  "CLIP_OF_THE_DAY":{
    ru:'Клип дня со звездами',
    en:'Music video of the day with the Stars'
  },
  "SEE_CLIP":{
    ru:'Смотри твой клип со звездой, зажимая кнопку',
    en:'Watch your music video with the Star keeping it pressed.'
  },
  "PARTICIPANTS_VIDEO":{
    ru:'ВИДЕО УЧАСТНИКОВ',
    en:'Participating videos'
  },
  "SORT_BY":{
    ru:'Сортировать по',
    en:'Sort by'
  },
  "POPULARITY":{
    ru:'популярности',
    en:'popularity'
  },
  "DATE":{
    ru:'Дате',
    en:'Date'
  },
  "YOU_VOTED":{
    ru:'УРА! ГОЛОС УЧТЕН!',
    en:'YOU VOTED'
  },
  "YOU_ALREADY_VOTED":{
    ru:'уже проголосовали',
    en:'YOU ALREADY VOTED'
  },
  "VOTE":{
    ru:'ПРОГОЛОСОВАТЬ',
    en:'VOTE'
  },
  "SHARE":{
    ru:'поделиться',
    en:'SHARE'
  },
  'COPY_LINK':{
    ru:'скопировать ссылку',
    en:'copy the link'
  },
  'COPIED':{
    ru:'СКОПИРОВАНО',
    en:'COPIED'
  },
  'TERMS':{
    ru:'Пользовательское&nbsp;соглашение',
    en:'Terms&nbsp;of&nbsp;use'
  },
  'POLICY':{
    ru:'Политика&nbsp;конфиденциальности',
    en:'Privacy&nbsp;Policy'
  },
  "BACKING_TRACK":{
    ru:'скачать музыку',
    en: 'backing track'
  },
  'SHOW_MORE':{
    ru:'ПОКАЗАТЬ ЕЩЕ',
    en:'SHOW MORE'
  },
  'SEARCH':{
    ru:'ПОИСК',
    en:'SEARCH',
  },
  'NOTHING_FOUND':{
    ru:'мы не нашли такого участника',
    en:'we didn\'t find such a participant'
  },
  'DOWNLOAD_CLIP':{
    ru:'Скачать клип',
    en:'download clip'
  }
  
}

export const lang = writable(document.querySelector('html').getAttribute('lang') || 'ru')
window.lang = lang;

export const text = derived(lang, $lang=>{
  return (key)=>translation[key][$lang]
})

