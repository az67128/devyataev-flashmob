export const IS_TEST = /(localhost|192\.168)/.test(window.location.href);

export const sendMetrik = (name, payload) => {
  const consoleMetrik = (id, type, name, payload) => console.log(name, payload);
  const ym = IS_TEST ? consoleMetrik : window.ym || consoleMetrik;
  // const ym = window.ym || consoleMetrik;

  ym(74334283, "reachGoal", `Devytaev.${name}`, payload);
};


function fallbackCopyTextToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.value = text;
  
  // Avoid scrolling to bottom
  textArea.style.top = "0";
  textArea.style.left = "0";
  textArea.style.position = "fixed";

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Fallback: Copying text command was ' + msg);
  } catch (err) {
    console.error('Fallback: Oops, unable to copy', err);
  }

  document.body.removeChild(textArea);
}
export function copyTextToClipboard(text) {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(text);
    return;
  }
  navigator.clipboard.writeText(text).then(function() {
    console.log('Async: Copying to clipboard was successful!');
  }, function(err) {
    console.error('Async: Could not copy text: ', err);
  });
}